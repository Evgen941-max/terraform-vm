Init bin-file:
    terraform plan -out=main.tfplan
Apply bin-file:
    terraform apply main.tfplan
Init delete bin-file:
    terraform plan -destroy -out=planfile
Apply delete bin-file:
    terraform apply planfile

When you run "terraform apply main.tfplan", you need add private ssh-key, you can use this command:
    terraform output -raw tls_private_key > id_rsa
    terraform output public_ip_address

And you can try connect to VM:
    ssh -i id_rsa azureuser@<public_ip_address>